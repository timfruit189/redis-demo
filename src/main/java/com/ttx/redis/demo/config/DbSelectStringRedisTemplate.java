//package com.ttx.redis.demo.config;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.data.redis.connection.RedisConnection;
//import org.springframework.data.redis.connection.jedis.JedisConnection;
//import org.springframework.data.redis.core.StringRedisTemplate;
//
///**
// * 来源 https://www.jianshu.com/p/9d78cd1c46b6
// *
// * 注入的LettuceConnectionFactory需要#setShareNativeConnection(false); 才可以调用Connection#select()选库
// *
// * @author TimFruit
// * @date 20-1-4 下午5:45
// */
//@Slf4j
//public class DbSelectStringRedisTemplate extends StringRedisTemplate {
//
//    public static ThreadLocal<Integer> REDIS_DB_INDEX = new ThreadLocal<Integer>(){
//        @Override
//        protected Integer initialValue() {
//            return 0;
//        }
//    };
//
//    @Override
//    protected RedisConnection preProcessConnection(RedisConnection connection, boolean existingConnection) {
//        try {
//            Integer dbIndex = REDIS_DB_INDEX.get();
//            //如果设置了dbIndex
//            if (dbIndex != null) {
//                if (connection instanceof JedisConnection) {
//                    if (((JedisConnection) connection).getNativeConnection().getDB() != dbIndex) {
//                        log.debug("选择redis库{}", dbIndex);
//                        connection.select(dbIndex);
//                    }
//                } else {
//                    log.debug("选择redis库{}", dbIndex);
//                    connection.select(dbIndex);
//                }
//            } else {
//                log.debug("选择redis库{}", 0);
//                connection.select(0);
//            }
//        } finally {
//            REDIS_DB_INDEX.remove();
//        }
//        return super.preProcessConnection(connection, existingConnection);
//    }
//}
