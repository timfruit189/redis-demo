package com.ttx.redis.demo.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

/**
 * @author TimFruit
 * @date 19-12-29 下午2:27
 */
public class Fastjson2JsonRedisSerializer<T> implements RedisSerializer<T> {

    private Class<T> targetType;
    private GenericFastjson2JsonRedisSerializer genericRedisSerializer=new GenericFastjson2JsonRedisSerializer();

    public Fastjson2JsonRedisSerializer(Class<T> targetType) {
        this.targetType=targetType;
    }

    @Override
    public byte[] serialize(T t) throws SerializationException {
        return genericRedisSerializer.serialize(t);
    }

    @Override
    public T deserialize(byte[] bytes) throws SerializationException {
        Object obj=genericRedisSerializer.deserialize(bytes);
        return genericRedisSerializer.deserialize(obj, targetType);
    }


    @Override
    public Class<?> getTargetType() {
        return targetType;
    }



}
