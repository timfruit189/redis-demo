package com.ttx.redis.demo.config;

import com.ttx.redis.demo.manager.RedisManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.net.UnknownHostException;

/**
 *
 * @see RedisProperties
 * @see RedisAutoConfiguration
 *
 * @author TimFruit
 * @date 19-12-27
 */
@Configuration
public class RedisConfig {


    /**
     * 默认key是String, value是json
     * @param redisConnectionFactory
     * @return
     */
    @Bean("defaultRedisTemplate")
    public RedisTemplate<String,Object> defaultRedisTemplate(RedisConnectionFactory redisConnectionFactory){
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory);


        //设置序列化
        RedisSerializer stringSerializer=new StringRedisSerializer();
        //本人偏好使用fastjson序列化成json
        RedisSerializer jsonSerializer=new GenericFastjson2JsonRedisSerializer();
        //官方提供jackson序列化成json
//        RedisSerializer jsonSerializer=new GenericJackson2JsonRedisSerializer();

        template.setKeySerializer(stringSerializer);
        template.setValueSerializer(jsonSerializer);

        template.setHashKeySerializer(stringSerializer);
        template.setHashValueSerializer(jsonSerializer);

        return template;
    }


    /**
     * key是String, value是json
     * @param defaultRedisTemplate
     * @return
     */
    @Bean("defaultRedisManager")
    public RedisManager<String, Object> defaultRedisManager(
            @Qualifier("defaultRedisTemplate") RedisTemplate<String,Object> defaultRedisTemplate){
        return new RedisManager<>(defaultRedisTemplate);
    }


    /**
     * key是String, value是json
     * 使用3号库
     * @param defaultRedisTemplate
     * @return
     */
    @Bean("default3RedisManager")
    public RedisManager<String, Object> default3RedisManager(
            @Qualifier("defaultRedisTemplate") RedisTemplate<String,Object> defaultRedisTemplate){
        RedisTemplate<String,Object> default3RedisTemplate=RedisDbSelectFactory.selectDb(defaultRedisTemplate, 3);
        return new RedisManager<>(default3RedisTemplate);
    }




    /**
     * key, value 都是字符串
     * @param stringRedisTemplate 由{@link RedisAutoConfiguration}实例化stringRedisTemplate
     * @return
     */
    @Bean("stringRedisManager")
    public RedisManager<String,String> stringRedisManager(
            @Qualifier("stringRedisTemplate") RedisTemplate<String,String> stringRedisTemplate){
        return new RedisManager<>(stringRedisTemplate);
    }


    /**
     * key, value 都是字符串
     * 使用3号库
     * @param stringRedisTemplate 由{@link RedisAutoConfiguration}实例化stringRedisTemplate
     * @return
     */
    @Bean("string3RedisManager")
    public RedisManager<String,String> string3RedisManager(
            @Qualifier("stringRedisTemplate") RedisTemplate<String,String> stringRedisTemplate){
        RedisTemplate<String,String> string3RedisTemplate=RedisDbSelectFactory.selectDb(stringRedisTemplate, 3);
        return new RedisManager<>(string3RedisTemplate);
    }

    @Bean("stringRedisTemplate")
    public StringRedisTemplate stringRedisTemplate(RedisConnectionFactory redisConnectionFactory)
            throws UnknownHostException {
        StringRedisTemplate template = new StringRedisTemplate();
        template.setConnectionFactory(redisConnectionFactory);
////      是否开启共享连接, 默认开启
//        ((LettuceConnectionFactory)redisConnectionFactory).setShareNativeConnection(false);
        return template;
    }


//    @Bean("dbSelectStringRedisTemplate")
//    public DbSelectStringRedisTemplate dbSelectStringRedisTemplate(
//            RedisConnectionFactory redisConnectionFactory){
//        DbSelectStringRedisTemplate selectStringRedisTemplate=new DbSelectStringRedisTemplate();
//        ((LettuceConnectionFactory)redisConnectionFactory).setShareNativeConnection(false);
//        selectStringRedisTemplate.setConnectionFactory(redisConnectionFactory);
//        return selectStringRedisTemplate;
//    }



}
