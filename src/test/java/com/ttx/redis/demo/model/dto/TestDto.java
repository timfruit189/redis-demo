package com.ttx.redis.demo.model.dto;

/**
 * @author TimFruit
 * @date 19-12-29 下午3:10
 */
public class TestDto {

    private String name;
    private Integer age;

    public TestDto() {
    }

    public TestDto(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
