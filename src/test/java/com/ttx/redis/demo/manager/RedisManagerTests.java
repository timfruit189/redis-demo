package com.ttx.redis.demo.manager;

import com.alibaba.fastjson.JSONObject;
import com.ttx.redis.demo.config.GenericFastjson2JsonRedisSerializer;
import com.ttx.redis.demo.config.RedisDbSelectFactory;
import com.ttx.redis.demo.config.RedisKeyConfig;
import com.ttx.redis.demo.model.dto.TestDto;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * @author TimFruit
 * @date 19-12-29 下午2:58
 */
@SpringBootTest
public class RedisManagerTests {

    @SpyBean
    @Qualifier("defaultRedisManager")
    RedisManager<String,Object> defaultRedisManager;

    //选择3号库
    @SpyBean
    @Qualifier("default3RedisManager")
    RedisManager<String, Object> default3RedisManager;

    @SpyBean
    @Qualifier("stringRedisManager")
    RedisManager<String,String> stringRedisManager;


    //选择3号库
    @SpyBean
    @Qualifier("string3RedisManager")
    RedisManager<String,String> string3RedisManager;






    @Test
    public void test(){
        stringRedisManager.set(RedisKeyConfig.TEST_STRING_KEY, "99999");
    }


    @Test
    public void testDefaultObject(){
        defaultRedisManager.getRedisTemplate().setEnableTransactionSupport(true);
        TestDto dto=new TestDto("timfruit", 99);
        String objKey=RedisKeyConfig.TEST_OBJECT_KEY;
        defaultRedisManager.set(objKey, dto);


        Object obj=defaultRedisManager.get(objKey);
        Assert.assertNotNull(obj);
        TestDto resultDto=GenericFastjson2JsonRedisSerializer.deserialize(obj, TestDto.class);

        Assert.assertEquals(dto.getName(), resultDto.getName());
        Assert.assertEquals(dto.getAge(), resultDto.getAge());

    }


//    @Test
//    public void shouldUse3Db(){
//        default3RedisManager.set("test:3:string:key", "3");
//
//        defaultRedisManager.set("test:0:string:key", "0");
//    }


    // 测试并发
    // 设置redis最大连接数为20, 设置lettuce连接池为非共享连接, 在连接池的作用下, 可以安全的运行下去, 不会报超出连接数的错误, 说明连接池生效
    // 设置共享连接, 会被非共享连接性能更好
    // 如果没有配置连接池, 在共享连接的情况下, 不会报超出连接数的错误, 否则会包超出连接数的错误
    @Test
    public void shouldUse3DbConcurrent(){
        final String key3="test:3:concurrent:string:key";
        final String key0="test:0:concurrent:string:key";

        List<String> keys=Arrays.asList(key0, key3);
        stringRedisManager.deleteKeyBatch(keys);
        string3RedisManager.deleteKeyBatch(keys);


        stringRedisManager.set(key0, "0");
        string3RedisManager.set(key3, "0");


        int threadCount=10;
        int addTotal=10000;
        RedisAddManager addManager0=new RedisAddManager(stringRedisManager, key0, threadCount, addTotal);
        RedisAddManager addManager3=new RedisAddManager(string3RedisManager, key3, threadCount, addTotal);


        addManager0.startThreads();
        addManager3.startThreads();


        addManager0.countDown();
        addManager3.countDown();


        //等待线程执行完毕
        addManager0.joins();
        addManager3.joins();

        int expectedTotal=threadCount*addTotal;
        String expectedTotalStr=String.valueOf(expectedTotal);

        String result0=stringRedisManager.get(key0);
        Assert.assertEquals(expectedTotalStr, result0);
        String result0_=stringRedisManager.get(key3);
        Assert.assertNull(result0_);

        String result3=string3RedisManager.get(key3);
        Assert.assertEquals(expectedTotalStr, result3);
        String result3_=string3RedisManager.get(key0);
        Assert.assertNull(result3_);

    }

    private class RedisAddManager{
        private CountDownLatch countDownLatch=new CountDownLatch(1);
        private RedisManager<String,String> stringRedisManager;
        private String key;

        private int threadCount;
        private int addTotal;

        private List<Thread> threads=new ArrayList<>();



        public RedisAddManager(RedisManager<String, String> stringRedisManager, String key, int threadCount, int addTotal) {
            this.stringRedisManager = stringRedisManager;
            this.key = key;
            this.threadCount=threadCount;
            this.addTotal = addTotal;
        }


        public void startThreads(){
            for(int i=0; i<threadCount;i++){
                Thread thread=new Thread(new RedisAddRunnable());
                threads.add(thread);

                thread.start();
            }

        }

        //释放启动所有线程
        public void countDown(){
            countDownLatch.countDown();
        }


        public void joins(){
            for(Thread thread: threads){
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }







        private class RedisAddRunnable implements Runnable{

            @Override
            public void run() {
                try {
                    countDownLatch.await();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println(Thread.currentThread().getId()+"开始计算");
                for(int i=0;i<addTotal;i++){
                    add(key, 1);
                }

            }


            public void add(String key, int delta){
                stringRedisManager.getRedisTemplate().opsForValue().increment(key, delta);
            }
        }


    }

//    @Autowired
//    DbSelectStringRedisTemplate selectStringRedisTemplate;
//    @Test
//    public void selectUse3Db(){
//        selectStringRedisTemplate.REDIS_DB_INDEX.set(3);
//        String key="test:3:template:string:key";
//
//        selectStringRedisTemplate.opsForValue().set(key, "3333333333333333");
//    }





}
