package com.ttx.redis.demo.lock;

import com.ttx.redis.demo.config.RedisKeyConfig;
import com.ttx.redis.demo.manager.RedisManager;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.integration.redis.util.RedisLockRegistry;
import org.springframework.util.Assert;

import java.util.concurrent.locks.Lock;

/**
 * @author TimFruit
 * @date 20-1-3 下午11:35
 */
@SpringBootTest
public class LockTests {
    @Autowired
    RedisLockRegistry defaultRedisLockRegistry;
    private final String lockKey ="test:lock:key";

    @Autowired
    RedisManager<String,String> stringRedisManager;

    @Test
    public void shouldLock(){

        //1分钟超时
        Lock lock=defaultRedisLockRegistry.obtain(lockKey);
        boolean locked=lock.tryLock();
        if(!locked){
            return;
        }

        //获取到锁
        String lockRedisKey=RedisKeyConfig.LOCK_DEFAULT_REGISTRY_KEY+":"+lockKey;
        try {
            //一般在try{}中写业务逻辑, 报错时, finally中可以释放锁
            String clientId=stringRedisManager.get(lockRedisKey);
            Assert.state(clientId!=null && clientId.length()>0, "没有获取到redis锁");
            System.out.println("获取到redis锁(clientId:"+clientId+")");


        }catch (Exception e){
            throw new RuntimeException(e);
        }finally {
            //释放锁, 有可能之前锁已超时,被清除, 然后报错
            lock.unlock();
        }


        String clientId=stringRedisManager.get(lockRedisKey);
        Assert.state(clientId==null, "锁没有被正常释放");

    }



}
